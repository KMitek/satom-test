<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(
    [
        'middleware' => ['auth:api']
    ],
    function () {
        Route::get('/department', 'DepartmentController@index')->name('departments');
        Route::get('/title', 'TitleController@index')->name('titles');
        Route::get(
            '/employee/{department}/{date}/{user_type}/{gender?}/{offset?}/{count?}',
            'EmployeeController@index'
        )
            ->name('employees')
            ->where(
                [
                    'department' => '^d[0-9]{3}$',
                    'date' => '^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$',
                    'user_type' => '^manager|employee$',
                    'gender' => '^F|M$',
                    'offset' => '[0-9]+',
                    'count' => '[0-9]+',
                ]
            );
        Route::post('/employee', 'EmployeeController@store')->name('store-employee');
    }
);
