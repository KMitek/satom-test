<?php
namespace App\Providers;

use Illuminate\Container\Container;
use Illuminate\Routing\ResponseFactory as Response;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Spatie\ArrayToXml\ArrayToXml;

/**
 * Based on https://github.com/mtownsend5512/response-xml
 * 
 */
class ResponseXmlServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Response::macro('xml', function ($xml, $status = 200, array $headers = [], $xmlRoot = 'response') {
            if (is_array($xml)) {
                $xml = ArrayToXml::convert($xml, $xmlRoot);
            } elseif (is_object($xml) && method_exists($xml, 'toArray')) {
                $xml = ArrayToXml::convert($xml->toArray(), $xmlRoot);
            } elseif (is_string($xml)) {
                $xml = $xml;
            } else {
                $xml = '';
            }
            if (!isset($headers['Content-Type'])) {
                $headers = array_merge($headers, ['Content-Type' => 'application/xml']);
            }
            return Response::make($xml, $status, $headers);
        });
        
        Response::macro('preferredFormat', function ($data, $status = 200, array $headers = [], $xmlRoot = 'response') {
            $request = Container::getInstance()->make('request');
            if (Str::contains($request->headers->get('Accept'), 'xml')) {
                if ($request->segment(1) === 'api' && $status === 200 ) {
                    $data = [($request->segment(2) ?? 'item') => $data];
                }
                return $this->xml($data, $status, array_merge($headers, ['Content-Type' => $request->headers->get('Accept')]), $xmlRoot);
            } else {
                return $this->json($data, $status, array_merge($headers, ['Content-Type' => $request->headers->get('Accept')]));
            }
        });
    }
}
