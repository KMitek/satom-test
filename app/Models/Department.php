<?php

namespace App\Models;

use App\Models\BaseModel;
use DB;

class Department extends BaseModel
{
    protected $primaryKey = 'dept_no';
    
    protected $keyType = 'string';
    
    /**
     * Get department managers.
     */
    public function manager()
    {
        return $this->belongsToMany('App\Models\Employee', 'dept_manager', 'dept_no', 'emp_no');
    }
    
    /**
     * Get department employees.
     */
    public function employee()
    {
        return $this->belongsToMany('App\Models\Employee', 'dept_emp', 'dept_no', 'emp_no');
    }
    
    /**
     * Returns a list of departments with the number of employees
     * and managers in each department.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function allWithCounts()
    {
        return self::withCount([
            'manager',
            'employee',
            'manager as current_manager_count' => self::makeOnlyCurrentClosure(),
            'employee as current_employee_count' => self::makeOnlyCurrentClosure(),
        ])->get()->toArray();
    }
    
    protected static function makeOnlyCurrentClosure()
    {
        return function ($query) {
            $query->where('to_date', '>', DB::raw('CURDATE()'));
        };
    }
}
