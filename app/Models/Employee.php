<?php

namespace App\Models;

use App\Models\BaseModel;

class Employee extends BaseModel
{
    protected $fillable = ['emp_no', 'first_name', 'last_name', 'gender', 'birth_date', 'hire_date'];
    
    protected $primaryKey = 'emp_no';
    
    /**
     * Get employee departments.
     */
    public function employeeDepartments()
    {
        return $this->belongsToMany('App\Models\Department', 'dept_emp', 'emp_no', 'dept_no')
            ->withPivot('from_date', 'to_date');
    }
    
    /**
     * Get manager departments.
     */
    public function managerDepartments()
    {
        return $this->belongsToMany('App\Models\Department', 'dept_manager', 'emp_no', 'dept_no')
            ->withPivot('from_date', 'to_date');
    }
    
    /**
     * Get employee title.
     */
    public function title()
    {
        return $this->hasMany('App\Models\Title', 'emp_no')->select('emp_no', 'title');
    }
    
    /**
     * Get employee salary.
     */
    public function salary()
    {
        return $this->hasMany('App\Models\Salary', 'emp_no')->select('emp_no', 'salary');
    }
    
    public static function getList($department, $date, $userType, $gender, $offset, $count) {
        $relation = $userType . 'Departments';
        
        $employeesList = self::select('emp_no', 'first_name', 'last_name', 'gender')
            ->with([
                'title' => self::makeDateClosure($date),
                'salary' => self::makeDateClosure($date)
            ])
            ->whereHas(
                $relation,
                function ($query) use ($department, $date) {
                    $query->where([
                        ['departments.dept_no', '=', $department],
                        ['from_date', '<=', $date],
                        ['to_date', '>', $date]
                    ]);
                }
            )
            ->when($gender, function ($query, $gender) {
                return $query->where('gender', $gender);
            })
            ->offset($offset)
            ->limit($count)
            ->get()
            ->toArray();
        
        return self::adjustListStructure($employeesList, $userType);
    }
    
    /**
     * 
     * @param array $employeesList
     * @return array
     */
    protected static function adjustListStructure($employeesList, $userType)
    {
        array_walk($employeesList, function (&$item, $key) use ($userType) {
            $item['title'] = $item['title'][0]['title'];
            $item['salary'] = $item['salary'][0]['salary'];
            $item['user_type'] = $userType;
            unset($item['emp_no']);
        });
        
        return $employeesList;
    }
    
    protected static function makeDateClosure($date)
    {
        return function ($query) use ($date) {
            $query->where([
                ['from_date', '<=', $date],
                ['to_date', '>', $date]
            ]);
        };
    }
    
    protected static function boot()
    {
        parent::boot();
        
        static::creating(function ($model) {
            // As employees.emp_no is non-auto-incrementing, we should set it
            // for new employee here
            $model->emp_no = self::max('emp_no') + 1;
        });
    }
}
