<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    protected $connection = 'mysql';
    
    public $incrementing = false;
    
    public $timestamps = false;
}
