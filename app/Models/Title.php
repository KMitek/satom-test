<?php

namespace App\Models;

use App\Models\BaseModel;

class Title extends BaseModel
{
    protected $fillable = ['emp_no', 'title', 'from_date', 'to_date'];
    
    /**
     * As Laravel doesn't support composite PK I should set it to null :(
     * 
     */
    protected $primaryKey = null;
}
