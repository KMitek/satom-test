<?php

namespace App\Models;

use App\Models\BaseModel;

class Salary extends BaseModel
{
    protected $fillable = ['emp_no', 'salary', 'from_date', 'to_date'];
    
    /**
     * As Laravel doesn't support composite PK I should set it to null :(
     * 
     */
    protected $primaryKey = null;
}
