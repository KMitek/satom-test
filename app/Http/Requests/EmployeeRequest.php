<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:14',
            'last_name' => 'required|string|max:16',
            'gender' => 'required|in:F,M',
            'birth_date' => 'required|date_format:Y-m-d',
            'hire_date' => 'required|date_format:Y-m-d',
            'department' => 'required|exists:departments,dept_no',
            'title' => 'required|exists:titles,title',
            'salary' => 'required|integer',
        ];
    }
}
