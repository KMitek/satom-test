<?php

namespace App\Http\Controllers;

use App\Models\Title;

class TitleController extends Controller
{
    /**
     * Returns a list of titles sorted alphabetically.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        return response()->preferredFormat(
            Title::select('title')
                ->orderBy('title')
                ->distinct()
                ->get()
                ->pluck('title')
                ->toArray()
        );
    }
}
