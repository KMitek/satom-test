<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Http\Requests\EmployeeRequest;

class EmployeeController extends Controller
{
    /**
     * Return a listing of the resource.
     * 
     * @param string $department department ID
     * @param string $date Date in 'YYYY-MM-DD' format
     * @param string $userType 'manager' or 'employee'
     * @param string $gender 'F'(female) or 'M'(male)
     * @param integer $offset
     * @param interer $count
     *
     * @return \Illuminate\Http\Response
     */

    public function index($department, $date, $userType, $gender = null, $offset = 0, $count = 100)
    {
        return response()->preferredFormat(
            Employee::getList($department, $date, $userType, $gender, $offset, $count)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $employee = Employee::create($request->all());
        
        $date = date('Y-m-d');
        $relationsData = [
            'emp_no' => $employee->emp_no,
            'from_date' => $date,
            'to_date' => '9999-01-01'
        ];
        
        $employee->salary()->create(
            array_merge($relationsData, ['salary' => request()->salary])
        );
        $employee->title()->create(
            array_merge($relationsData, ['title' => request()->title])
        );
        
        $employee->employeeDepartments()->attach(
            request()->department,
            $relationsData
        );
        
        return response()->preferredFormat(
            ['message' => 'Created.']
        );
    }

}
