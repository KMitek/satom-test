<?php

namespace Tests\Feature;

use Tests\TestCase;

class DepartmentTest extends TestCase
{
    /**
     * Test response json structure for departments list.
     *
     * @return void
     */
    public function testDepartmentsList()
    {
        $this->createTestUser('api/department');
        $response = $this->json('GET', 'api/department');
        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    '*' => [
                        'dept_no',
                        'dept_name',
                        'manager_count',
                        'employee_count',
                        'current_manager_count',
                        'current_employee_count',
                    ]
                ]
            );
    }
}
