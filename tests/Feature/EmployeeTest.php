<?php

namespace Tests\Feature;

use Tests\TestCase;

class EmployeeTest extends TestCase
{
    /**
     * Test response json structure for employee list.
     *
     * @return void
     */
    public function testEmployeeList()
    {
        $this->createTestUser('employees');
        $response = $this->json('GET', 'api/employee/d009/1990-05-28/manager/F/0/1');
        $response
            ->assertStatus(200)
            ->assertJson(
                [
                    [
                        "first_name" => "Marjo",
                        "last_name" => "Giarratana",
                        "gender" => "F",
                        "title" => "Manager",
                        "salary" => 44026,
                        "user_type" => "manager"
                    ]
                ]
            );
    }
}
