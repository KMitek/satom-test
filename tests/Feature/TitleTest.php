<?php

namespace Tests\Feature;

use Tests\TestCase;

class TitleTest extends TestCase
{
    /**
     * Test response json structure for titles list.
     *
     * @return void
     */
    public function testTitleList()
    {
        $this->createTestUser('api/title');
        $response = $this->json('GET', 'api/title');
        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                []
            );
    }
}
