<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Str;

class BaseApiTest extends TestCase
{
    /**
     * Test response status and message for unauthenticated requests.
     *
     * @return void
     */
    public function testUnauthenticated()
    {
        $response = $this->json('GET', 'api/department');
        $response
            ->assertStatus(401)
            ->assertJson(["error" => "Unauthenticated."]);
    }
    
        
    /**
     * Test response formats.
     *
     * @return void
     */
    public function testResponseFormats()
    {
        $this->createTestUser('api/*');
        $this->checkResponseFormat('json');
        $this->checkResponseFormat('xml');
    }
    
    /**
     * Test response format depending on request 'Accept' header.
     *
     * @return void
     */
    protected function checkResponseFormat($format)
    {
        $response = $this->get(
            'api/department',
            ['Accept' => 'application/' . $format]
        );
        
        $contentTypeHeader = $response->headers->get('content-type');
        $this->assertTrue(
            Str::contains($contentTypeHeader, $format)
        );
    }
}
