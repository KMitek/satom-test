## Установка и настройка
1. Клонировать репозиторий `git clone https://KMitek@bitbucket.org/KMitek/satom-test.git`
2. Установить базу данных Employees Sample Database (https://dev.mysql.com/doc/employee/en/)
3. Скопировать и переименовать .env.example в .env
4. Прописать в .env соединение к базе данных employees
5. Выполнить команды:
	* `composer install`
	* `php artisan migrate`
	* `php artisan key:generate`
	* `php artisan passport:install`
6. Зарегистрировать нового пользователя http://your-app.com/register
7. Получить токен для доступа к api.  
Для аутентификации используется стандартный пакет Laravel Passport. Ниже описаны 3 способа получения токена: 
	* Можно получить, например, "Password Grant Token" следуя инструкциям - https://laravel.com/docs/5.7/passport#password-grant-tokens  
	* Запрос токена "Password Grant Token" при помощи Postman:
		* Выбрать Authorization
		* Выбрать TYPE - OAuth 2.0
		* Затем нажать 'Get New Access Token'
			* Grant Type: Password Credentials
			* Access Token URL: http://your-app.com/oauth/token
			* Username, Password - те, что были указаны в пункте 6 (при регистрации нового пользователя на странице http://your-app.com/register)
			* Client ID, Client Secret - в табличке oauth_clients - клиент под именем 'Laravel Password Grant Client'
			* Нажать 'Request Token'
 	* Запрос токена "Password Grant Token" через cURL:  
	`curl -X POST -H 'Content-Type: application/form-data' -F 'grant_type=password' -F 'client_id=client-id' -F 'client_secret=client-secret' -F 'username=user-name' -F 'password=PASSWORD' -F 'scope=' http://your-app.com/oauth/token`  
	client-id, client-secret - в табличке oauth_clients - клиент под именем 'Laravel Password Grant Client'  
	PASSWORD, user-name - те, что были указаны в пункте 6  
	your-app.com - поменять на свой хост

Если не хочется заморачиваться с токеном, можно просто закомментить строку, содержащую `'middleware' => ['auth:api']`, в routes/api.php (в этом случае перестанут проходить некоторые тесты)


## Комментарии к пунктам задания
1. GET http://your-app.com/api/department  
Поскольку из задания непонятно, нужно ли общее количество всех менеджеров и служащих, или только тех, которые работают в департаменте в текущий момент,  
то решил выдавать и то и другое значение:  
manager_count, employee_count - общее колличество  
current_manager_count, current_employee_count - только те, которые работают в текущий момент
2. GET http://your-app.com/api/title
3. GET http://your-app.com/api/employee/d001/1995-01-07/manager/F/0/100
4. POST http://your-app.com/api/employee  
	Пример cURL запроса добавления сотрудника:  
	`curl -d "first_name=aaa&last_name=bbb&gender=F&birth_date=1970-10-10&hire_date=2010-10-10&department=d001&title=Staff&salary=100" -H "Content-Type: application/x-www-form-urlencoded" -H "Authorization: Bearer YOUR-ACCESS-TOKEN" -X POST http://your-app.com/api/employee`  
	YOUR-ACCESS-TOKEN - подставьте access_token, полученный в 7 пункте инструкции по установке и настройке  
	your-app.com - поменять на свой хост

Формат возвращаемых данных (json/xml) у меня определяется заголовком 'Accept' запроса. Если критично, можно переделать на 'Content-type'.  
Просто у 'Content-type' смысл немного другой. Он используется для указания формата содержимого, которое отправляется в запросе (или ответе)

## Запуск тестов
Выполнить комадну `phpunit`из корневой директории проекта